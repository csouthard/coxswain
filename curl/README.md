# Useful GitLab API Requests

## Total Closed Utilization Issues in Milestone 17.1

```
curl -I --header "PRIVATE-TOKEN: itsasecret" "https://gitlab.com/api/v4/issues?milestone=17.1&labels=group%3A%3Autilization&state=closed&scope=all"
```

## Total Weight: All Closed Utilization Issues in Milestone 16.10

### Requirements
- curl
- jq (brew install jq)
- GitLab API token

```
curl "https://gitlab.com/api/v4/issues?state=closed&scope=all&milestone=16.10&labels=group%3A%3Autilization" \
     -H 'PRIVATE-TOKEN: itsasecret' \
     | jq -r '[.[] | select(.weight!=null) | .weight | tonumber] | add'
```

## Total Weight: All Open Utilization Issues in Milestone 16.10

### Requirements
- curl
- jq (brew install jq)
- GitLab API token

```
curl "https://gitlab.com/api/v4/issues?state=open&scope=all&milestone=16.10&labels=group%3A%3Autilization" \
     -H 'PRIVATE-TOKEN: itsasecret' \
     | jq -r '[.[] | select(.weight!=null) | .weight | tonumber] | add'
```
