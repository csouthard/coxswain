require_relative "../spillover"

RSpec.describe Spillover::Issues::Client do
  describe "#labels" do
    let(:client) { Spillover::Issues::Client.new("15.1", "devops::fulfillment", "group::utilization", "") }

    it "returns a comma separated list of labels from attributes" do
      expect(client.labels).to eq("devops::fulfillment,group::utilization")
    end
  end

  describe "#additional_query_string_params_from_attrs" do
    let(:client) { Spillover::Issues::Client.new("15.1", "devops::fulfillment", "group::utilization", "") }

    it "builds an HTML escaped query string of labels and milestone attributes" do
      expect(client.additional_query_string_params_from_attrs).to eq("labels=devops::fulfillment,group::utilization&milestone=15.1")
    end
  end
end
