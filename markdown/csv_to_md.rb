#!/usr/bin/env ruby
require "bundler/setup"
require "json"
require "csv"

# yeah, monkey patching CSV::Row 🙈
class CSV::Row
  def to_md_link
    "[#{fetch("Title")}](#{fetch("URL")})"
  end
end

CSV.foreach(ARGV[0], headers: true) { |row| puts row.to_md_link }
