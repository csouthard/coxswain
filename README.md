```
cox·swain
/ˈkäks(ə)n/

noun
the steersman of a ship's boat, lifeboat, racing boat, or other boat.
```

#### Description

In rowing/crew, there's a person at the back of the boat who coordinates the power and rhythm of the team. Here, I'm assembling some scripts to aid in my team coordination.

#### Usage

##### General Requirements

* Ruby Ruby 3+ (ideally Ruby 3.3.x)
* GitLab API key (personal access token) with rights to read issues and merge requests

##### Getting Started

* copy the .env.template to .env and replace the PRIVATE_TOKEN with your own GitLab API token. That token will need to have rights to issues and merge requests.
* run the getting_started script to install pre-commit hooks and run bundle command `./bin/get_started`
* check the READMEs for the individual scripts for additional requirements.

**Structure**

Each tool is contained in it's own directory. Most of these are one-off experimental tools that were put together quickly. As a result, there is some variability in stylistic conventions and dependency management where some projects may have a separate `Gemfile`.

#### Individual tools

* [spillover](spillover/): Getting issues that have spilled over for a given milestone
* [capacity](bin/capacity/): get a list of issues, weights, and other milestone data
* [maintainers](maintainers/): IRB session for analyzing team member files that roughly counts maintainers
* [log_parser](log_parser/): information processing JSON logs and conversion tools to CSV
* [milestones](milestones/): various outdated scripts to format a list of issues by workflow label
* [markdown](markdown/): various small markdown tools to avoid tedium
* [team metrics](team_metrics/): unofficial rate  inspection using the GitLab API
* [weekly report](weekly_report/): display list issues in markdown that have been closed within a certain time frame

#### Style, Linting, and Formatting

In an effort to maintain some consistency across these one-offs and bespoke mini-tools, the project relies on the [Standard](https://github.com/testdouble/standard) gem. It's part of the main [Gemfile](./Gemfile) and with the `get_started` script is it called from a pre-commit hook.
