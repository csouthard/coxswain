# weekly report

This script will display list Issues in markdown that have been closed within a certain time frame.

TODO: 
- add Merge Requests that have been merged
- format overview results in a table 

## requirements

- Ruby 3+, preferably Ruby 3.3.x, bundler,
- GitLab API key with `read_api` access

## usage

- if you already haven't done so, `cp ../env.template .env` and update with your GitLab API key
- install dependencies with `bundle install`
- update [config.toml](./config.toml) to add a team following the example structure. Section and Group labels are required. e.g. "section::fulfillment" and "group::utilization"
- Run: `ruby ./report` and follow the prompts
