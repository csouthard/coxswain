require "spec_helper"
require_relative "../../lib/gitlab_teams"

RSpec.describe GitlabTeams do
  let(:dummy_class) { Class.new { extend GitlabTeams } }

  describe "#sections" do
    it "returns an array of sections" do
      expect(dummy_class.sections).to be_kind_of(Array)
    end

    it "has a :name key" do
      expect(dummy_class.sections.first.key?(:name)).to be_truthy
      expect(dummy_class.sections.first[:name]).to be_kind_of(String)
    end

    it "has a :teams key" do
      expect(dummy_class.sections.first.key?(:teams)).to be_truthy
      expect(dummy_class.sections.first[:teams]).to be_kind_of(Array)
    end
  end
end
