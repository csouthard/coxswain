require "spec_helper"
require_relative "../../lib/utils"

RSpec.describe Utils do
  describe "format_http_params" do
    describe "simple parameters" do
      it "formats a hash for http" do
        hsh = {foo: "bar"}
        expect(Utils.format_http_params(hsh)).to eq("foo=bar")
      end
    end

    describe "real world but simple params" do
      it "formats a hash for http" do
        hsh = {per_page: 100}
        expect(Utils.format_http_params(hsh)).to eq("per_page=100")
      end
    end

    describe "multiple params" do
      it "formats a hash for http with & between params" do
        hsh = {per_page: 100, page: 1}
        expect(Utils.format_http_params(hsh)).to eq("per_page=100&page=1")
      end
    end

    describe "multiple params with different hash forms" do
      it "formats a hash for http with & between params" do
        hsh = {:per_page => 100, :page => 1, "bing" => "bong", :word => "blarg"}
        expect(Utils.format_http_params(hsh)).to eq("per_page=100&page=1&bing=bong&word=blarg")
      end
    end
  end
end
