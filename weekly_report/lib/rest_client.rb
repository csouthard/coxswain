require_relative "utils"

module RestClient
  VERBOSE_MODE = ENV["VERBOSE"] || "false"

  def get_paginated_results(path, query_params, headers)
    Enumerator.new do |yielder|
      page = 1

      loop do
        results = get(path, page, query_params, headers)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          raise StopIteration
        end
      end
    end.lazy
  end

  def get(path, page = 1, query_params = {}, headers = {})
    query_options = base_options.merge(page: page).merge(query_params)
    response = HTTParty.get(full_path(path, query_options),
      follow_redirects: true,
      maintain_method_across_redirects: true,
      headers: headers)
    puts response.body if VERBOSE_MODE == "true"
    response
  end

  def base_options
    {per_page: 100, page: 1}
  end

  def full_path(path, query_options)
    path = "#{path}&page=#{query_options[:page]}&#{Utils.format_http_params(query_options)}"
    if VERBOSE_MODE == "true"
      puts path if ENV.keys("VERBOSE")
    end
    path
  end
end
