require "bundler/setup"
require "dotenv"
require "httparty"
require "erb"
require "date"
require_relative "rest_client"
require_relative "utils"

class Milestone
  attr_accessor :id, :iid, :title, :due_date, :start_date, :state

  def initialize(id, iid, title, state, start_date, due_date)
    @id = id
    @iid = iid
    @title = title
    @state = state
    @start_date = start_date
    @due_date = due_date
  end

  class << self
    Dotenv.load(File.expand_path("../../.env", __dir__))

    Dotenv.require_keys("PRIVATE_TOKEN")

    API_KEY = ENV.fetch("PRIVATE_TOKEN")
    BASE_URL = "https://gitlab.com/api/v4/groups/9970/milestones?"
    BASE_HEADERS = {"PRIVATE-TOKEN" => API_KEY}

    def active
      milestone_data ||= JSON.parse(get_milestones(BASE_URL, {"state" => "active"}, BASE_HEADERS).response.body)
      milestone_data.map { |md| new(md["id"], md["iid"], md["title"], md["state"], md["start_date"], md["due_date"]) }
    end

    def current
      now = DateTime.now
      active.find { |milestone| !milestone.due_date.nil? && Date.parse(milestone.start_date) <= now && Date.parse(milestone.due_date) >= now }
    end

    def find(title)
      active.find { |milestone| milestone.title == title }
    end

    private

    def get_milestones(path, query_params = {}, headers = {})
      response ||= HTTParty.get(full_path(path, query_params),
        follow_redirects: true,
        maintain_method_across_redirects: true,
        headers: headers)
      # puts response.body if VERBOSE_MODE == "true"
      begin
        case response.code
        when 200
          response
        when 401
          puts response.inspect
          raise "Unauthorized"
        when 404
          puts response.inspect
          raise "Not Found"
        when 500
          puts response.inspect
          raise "Internal Server Error"
        when 502
          puts response.inspect
          raise "Bad Gateway"
        when 503
          puts response.inspect
          raise "Service Unavailable"
        when 504
          puts response.inspect
          raise "Gateway Timeout"
        else
          puts response.inspect
          raise "Unknown Error"
        end
      rescue HTTParty::Error, SocketError, Timeout::Error => exception
        puts "Error: #{exception.inspect}"
      end
    end

    def full_path(path, query_options)
      "#{path}&#{Utils.format_http_params(query_options)}"
      # puts path if ENV.keys("VERBOSE") if VERBOSE_MODE == "true"
    end
  end

  def to_s
    @title
  end
end
