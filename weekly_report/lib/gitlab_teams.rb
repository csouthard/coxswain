require "bundler/setup"
require "perfect_toml"

module GitlabTeams
  def sections
    config[:sections]
  end

  def teams(section)
    config[:sections].filter { |s| s[:name] == section }&.first&.fetch(:teams, "")
  end

  private

  def config
    @config ||= PerfectTOML.load_file(File.expand_path("../config.toml", __dir__), symbolize_names: true)
  end
end
