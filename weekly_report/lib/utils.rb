module Utils
  def self.format_http_params(params_hash)
    params_hash.map { |k, v| "#{k}=#{v}" }.join("&")
  end
end
