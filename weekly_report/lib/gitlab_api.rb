require "bundler/setup"
require "dotenv"
require "httparty"
require "erb"
require_relative "rest_client"

module GitlabApi
  module Issues
    class Client
      include RestClient
      Dotenv.load(File.expand_path("../../.env", __dir__))

      Dotenv.require_keys("PRIVATE_TOKEN")

      API_KEY = ENV.fetch("PRIVATE_TOKEN")
      BASE_URL = "https://gitlab.com/api/v4/groups/9970/issues?"
      BASE_QUERY = {
        # "author_username" => "",
        "updated_after" => "", # 2020-01-01T00:00:00Z
        "scope" => "all"
      }
      BASE_HEADERS = {"PRIVATE-TOKEN" => API_KEY}

      def initialize(stage = "section::fulfillment", team = "group::utilization", other = "", options = {})
        @stage = stage
        @team = team
        @other = other
        @options = options
        @data = []
      end

      def collect_issues
        get_paginated_results(BASE_URL, compute_query_params, BASE_HEADERS)
      end

      def compute_query_params
        BASE_QUERY.merge({"labels" => [@stage, @team, @other].compact.join(",")}, @options)
      end
    end
  end
end
