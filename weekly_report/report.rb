#!/usr/bin/env ruby

require "bundler/setup"
require "httparty"
require "date"
require "csv"
require "tty-table"
require "tty-prompt"
require "rainbow/refinement"
require "perfect_toml"
require "logger"
require_relative "lib/gitlab_teams"
require_relative "lib/gitlab_api"
require_relative "lib/milestone"

using Rainbow

class Report
  include GitlabTeams

  def initialize
    @prompt = TTY::Prompt.new(interrupt: :exit)
    @responses = {section: "", team: "", duration: 7, labels: ""}
    @logger = Logger.new("report.log")
  end

  Issue = Struct.new(:iid, :project_id, :title, :web_url, :labels, :assignees, :weight)

  def run!
    @logger.info("WWW - Starting Report: #{Date.today} - WWW")

    @responses[:section] = section_prompt
    @responses[:team] = team_prompt(@responses[:section])
    @responses[:milestone] = milestone_prompt
    @responses[:duration] = days_since_prompt.to_i
    @responses[:other_label] = other_label_prompt
    @responses[:issue_state] = issue_state_prompt

    @logger.debug(@responses.inspect)

    @prompt.say("Gathering data for #{@responses[:team]} ...")

    # fetch issues --> labels = [section, team], updated_after: Timestamp days ago from duration, state: closed, scope: all
    since = (DateTime.now - @responses[:duration]).iso8601

    @logger.info("Connecting to GitLab")
    client = GitlabApi::Issues::Client.new(@responses[:section], @responses[:team], @responses[:other_label], {"updated_after" => since, "milestone" => @responses[:milestone], "state" => @responses[:issue_state]})
    issues = client.collect_issues.map { |response| Issue.new(response["iid"], response["project_id"], response["title"], response["web_url"], response["labels"], response["assignees"], response["weight"]) }.to_a

    table = TTY::Table.new([["Issue Count", issues.count], ["Total Weight", issues.map { |i| i.weight.to_i }.reduce(0, :+)]])
    renderer = TTY::Table::Renderer::Basic.new(table)
    puts "\n#{@responses[:issue_state].capitalize} Issues Overview: \n"
    puts "-------------"
    puts renderer.render
    puts "-------------"

    puts "\n#{@responses[:issue_state].capitalize} Issues: \n"
    issues.each do |issue|
      puts "- [ ] #{issue["title"]} - #{issue["web_url"]} (#{issue["labels"]}) [#{issue["assignees"].map { |assingee| assingee["username"] }.join(", ")}] weight: #{issue["weight"]}"
    end
    # csouthard(TODO): also fetch closed mrs during the same period
    # csouthard(TOOD): display markdown table of results

    @logger.info("MMM - Finished Report: #{Date.today} - MMM")
  end

  private

  def questions(key)
    {
      section: "Choose your section:",
      team: "Choose your team(s):",
      duration: "How many days would you like to look back?",
      milestone: "Choose an active milestone?",
      other_label: "Enter another label (optional)",
      issue_state: "What issues states are you looking for?"
    }.fetch(key)
  end

  def section_prompt
    @prompt.select(questions(:section), cycle: true) do |q|
      q.choices(sections)
      q.default(1)
    end
  end

  def team_prompt(section)
    @prompt.multi_select(questions(:team), cycle: true, show_help: :always) do |q|
      q.choices(teams(section))
      q.default(1)
    end
  end

  def days_since_prompt
    selected_milestone = Milestone.find(@responses[:milestone])
    days_ago = (DateTime.now - DateTime.parse(selected_milestone.start_date)).to_i
    @prompt.say("#{@responses[:milestone]} began on #{selected_milestone.start_date} which was #{days_ago} days ago.")
    @prompt.ask(questions(:duration), default: 7) do |q|
      q.in("1-365")
      q.required(true)
    end
  end

  def milestone_prompt
    @prompt.select(questions(:milestone), cycle: true) do |q|
      q.choices(Milestone.active.map(&:title))
      q.default(Milestone.current.title)
    end
  end

  def other_label_prompt
    @prompt.ask(questions(:other_label))
  end

  def issue_state_prompt
    @prompt.select(questions(:issue_state), cycle: true) do |q|
      q.default("closed")
      q.choices(["all", "closed", "opened"])
    end
  end
end

Report.new.run!
