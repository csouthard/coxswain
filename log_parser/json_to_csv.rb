#!/usr/bin/env ruby
require "bundler/setup"
require "json"
require "csv"

CSV.open("filter_list_w_amendable.csv", "w") do |csv|
  JSON.parse(File.read("filtered_list_w_amendable_objects.json")).each do |hash|
    csv << hash.values
  end
end
