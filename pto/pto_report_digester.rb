# frozen_string_literal: true

require "date"
require "csv"

module PTO
  class ReportDigester # rubocop:disable Style/Documentation
    attr_accessor :data, :days, :year, :outfile

    Day = Struct.new(:date, :event_count)
    Year = Struct.new(:year, :days)

    def initialize(infile, outfile = "outfile.csv")
      @outfile = outfile
      @data = pto_data(infile)
      @year = build_year(oldest_start_date, neweset_end_date)
      tabulate_pto
    end

    def to_csv
      headers = ["Date", "Event Count"]
      CSV.open(@outfile, "w") do |csv|
        csv << headers
        @year.days.each do |day|
          csv << [day.date, day.event_count]
        end
      end
    end

    private

    def tabulate_pto
      @data.each_with_index do |row, _i|
        days_between(row["Start Date"], row["End Date"]).each do |day|
          increment_daily_events(day)
        end
      end
    end

    def pto_data(path)
      return if IO.popen(["file", "--brief", "--mime-type", path], &:read).chomp != "text/csv"

      CSV.parse(File.open(path), headers: true)
    end

    def oldest_start_date
      @data.map { |row| Date.parse(row["Start Date"]) }.min
    end

    def neweset_end_date
      @data.map { |row| Date.parse(row["End Date"]) }.max
    end

    def increment_daily_events(date)
      @year.days.find { |d| d.date == date }.event_count += 1
    end

    def days_between(start_date, end_date)
      Date.parse(start_date)..Date.parse(end_date)
    end

    def build_year(start_date = Date.new(2023, 1, 1), end_date = Date.new(2023, 12, 31))
      Year.new(year, build_days(start_date..end_date))
    end

    def build_days(all_days)
      proto_year = all_days.map do |day|
        Day.new(day, 0)
      end
      fill_in_missing_days(proto_year)
      proto_year
    end

    def full_year?(end_date)
      end_date.month == 12 && end_date.day == 31
    end

    def fill_in_missing_days(days)
      return if full_year?(days.last.date)
      until full_year?(days.last.date)
        days << Day.new(days.last.date + 1, 0)
      end
    end
  end
end
