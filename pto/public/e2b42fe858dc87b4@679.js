function _1(md){return(
md`# Utilization PTO Calendar Heat Map

This notebook uses a [D3-style calendar](https://observablehq.com/@d3/calendar/2) from the [Observable Plot calendar example](https://observablehq.com/@observablehq/plot-calendar?intent=fork).

<!-- This implementation features a custom transform to provide default **x** (week number), **y** (weekday number), and **fy** (year) options derived from the date;
it also uses a custom mark to delineate months. -->

Last Update: 2024-05-17
`
)}

function _plot_pto(d3,pto,Plot,calendar,MonthLine)
{
  const start = d3.utcDay.offset(d3.min(pto, (d) => d.Date)); // exclusive
  const end = d3.utcDay.offset(d3.max(pto, (d) => d.Date)); // exclusive
  return Plot.plot({
    width: 1152,
    height: d3.utcYear.count(start, end) * 160,
    axis: null,
    padding: 0,
    x: {
      domain: d3.range(53) // or 54, if showing weekends
    },
    y: {
      axis: "left",
      domain: [-1, 1, 2, 3, 4, 5], // hide 0 and 6 (weekends); use -1 for labels
      ticks: [1, 2, 3, 4, 5], // don’t draw a tick for -1
      tickSize: 0,
      tickFormat: Plot.formatWeekday()
    },
    fy: {
      padding: 0.1,
      reverse: true
    },
    color: {
      scheme: "Oranges",
      domain: [0, 6],
      legend: true,
      percent: false,
      ticks: 7,
      tickFormat: "+d",
      label: "PTO Event Count"
    },
    marks: [
      // Draw year labels, rounding down to draw a year even if the data doesn’t
      // start on January 1. Use y = -1 (i.e., above Sunday) to align the year
      // labels vertically with the month labels, and shift them left to align
      // them horizontally with the weekday labels.
      Plot.text(
        d3.utcYears(d3.utcYear(start), end),
        calendar({text: d3.utcFormat("%Y"), frameAnchor: "right", x: 0, y: -1, dx: -20})
      ),

      // Draw month labels at the start of each month, rounding down to draw a
      // month even if the data doesn’t start on the first of the month. As
      // above, use y = -1 to place the month labels above the cells. (If you
      // want to show weekends, round up to Sunday instead of Monday.)
      Plot.text(
        d3.utcMonths(d3.utcMonth(start), end).map(d3.utcMonday.ceil),
        calendar({text: d3.utcFormat("%b"), frameAnchor: "left", y: -1})
      ),

      // Draw a cell for each day in our dataset. The color of the cell encodes
      // the number of team members on PTO.
      Plot.cell(
        pto.slice(1),
        calendar({date: "Date", fill: Plot.valueof(pto, "Event Count")})
      ),

      // Draw a line delineating adjacent months. Since the y-domain above is
      // set to hide weekends (day number 0 = Sunday and 6 = Saturday), if the
      // first day of the month is a weekend, round up to the first monday.
      new MonthLine(
        d3.utcMonths(d3.utcMonth(start), end)
          .map((d) => d3.utcDay.offset(d, d.getUTCDay() === 0 ? 1
             : d.getUTCDay() === 6 ? 2
             : 0)),
        calendar({stroke: "white", strokeWidth: 3})
      ),

      // Lastly, draw the date for all days spanning the dataset, including
      // days for which there is no data.
      Plot.text(
        d3.utcDays(start, end),
        calendar({text: d3.utcFormat("%-d")})
      )
    ]
  });
}


function _pto(FileAttachment){return(
FileAttachment("outfile3.csv").csv({typed: true})
)}

function _calendar(Plot,d3){return(
function calendar({
  date = Plot.identity,
  inset = 0.5,
  ...options
} = {}) {
  let D;
  return {
    fy: {transform: (data) => (D = Plot.valueof(data, date, Array)).map((d) => d.getUTCFullYear())},
    x: {transform: () => D.map((d) => d3.utcWeek.count(d3.utcYear(d), d))},
    y: {transform: () => D.map((d) => d.getUTCDay())},
    inset,
    ...options
  };
}
)}

function _MonthLine(Plot,htl){return(
class MonthLine extends Plot.Mark {
  static defaults = {stroke: "currentColor", strokeWidth: 1};
  constructor(data, options = {}) {
    const {x, y} = options;
    super(data, {x: {value: x, scale: "x"}, y: {value: y, scale: "y"}}, options, MonthLine.defaults);
  }
  render(index, {x, y}, {x: X, y: Y}, dimensions) {
    const {marginTop, marginBottom, height} = dimensions;
    const dx = x.bandwidth(), dy = y.bandwidth();
    return htl.svg`<path fill=none stroke=${this.stroke} stroke-width=${this.strokeWidth} d=${
      Array.from(index, (i) => `${Y[i] > marginTop + dy * 1.5 // is the first day a Monday?
          ? `M${X[i] + dx},${marginTop}V${Y[i]}h${-dx}`
          : `M${X[i]},${marginTop}`}V${height - marginBottom}`)
        .join("")
    }>`;
  }
}
)}

export default function define(runtime, observer) {
  const main = runtime.module();
  function toString() { return this.url; }
  const fileAttachments = new Map([
    ["outfile3.csv", {url: new URL("./files/7751cdc285c5ba591b2ebdffc7567d191e1958476db195fdaae3ea63d690d7206987244491d4cf2a58a886d71c3f36c3717a4e8de428f33e6b8a9013dc8ac855.csv", import.meta.url), mimeType: "text/csv", toString}]
  ]);
  main.builtin("FileAttachment", runtime.fileAttachments(name => fileAttachments.get(name)));
  main.variable(observer()).define(["md"], _1);
  main.variable(observer("plot_pto")).define("plot_pto", ["d3","pto","Plot","calendar","MonthLine"], _plot_pto);
  main.variable(observer("pto")).define("pto", ["FileAttachment"], _pto);
  main.variable(observer("calendar")).define("calendar", ["Plot","d3"], _calendar);
  main.variable(observer("MonthLine")).define("MonthLine", ["Plot","htl"], _MonthLine);
  return main;
}
