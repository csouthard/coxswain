# PTO Report Digest

## Overview

Managers can get a detailed report of PTO from the `Time Off by Deel` slack app.
The resulting CSV is a detailed account for each team member. This report has
marginal value on its own. Some other visualization of current and future PTO may be
beneficial. You can generate a Heat Map of the results across the year.
See this example: https://observablehq.com/@chase-southard-ws/pto-calendar-heat-map

![](./images/plot_pto.png)

The `pto_report_digester` class attempts take the PTO report
(e.g. GitLabOOOEvents(2023-01-01 through 2023-12-26, Days).csv) and builds a
simplified CSV with two columns `Date, Event Count` where `Event Count` is the count
of team members on PTO for a given day.

```
Date,Event Count
2022-11-14,1
2022-11-15,1
2022-11-16,1
2022-11-17,1
...
```

HTML views and JS code were exported from [this Observable notebook](https://observablehq.com/@chase-southard-ws/pto-calendar-heat-map).
It uses a fork of the [Observable Plot calendar example](https://observablehq.com/@observablehq/plot-calendar?intent=fork).

## Limitations

- Partial days are counted as whole days
- All PTO types are included. There is not data slice for just Vacation or other types.

## Usage

```./bin/digest /path/to/file/GitLabOOOEvents(2023-01-01 through 2023-12-26, Days).csv```

## Publishing Output

- Get a PTO report for the current calendar year from Time Off by Deel
- Digest that report into daily counts `./bin/digest /path/to/file/GitLabOOOEvents(2023-01-01 through 2023-12-26, Days).csv`
- Copy results into `./public/files` (you may need to adjust the file references in to match `public/e2b42fe858dc87b4@679.js`).
- Local: Run a small web server (e.g., `python3 -m http.server` or `npx serve`)
- GitLab pages: visit https://csouthard.gitlab.io/coxswain/



