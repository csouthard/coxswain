module MRHarvester
  require "addressable/uri"
  require "httparty"

  API_KEY = ENV.fetch("GITLAB_API_KEY")
  BASE_HEADERS = {
    "PRIVATE-TOKEN" => API_KEY,
    "Content-Type" => "application/json"
  }

  def harvest_related_merged_requests(project_id, issue_id)
    url = "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_id}/related_merge_requests"

    HTTParty.get(url,
      follow_redirects: true,
      maintain_method_across_redirects: true,
      headers: BASE_HEADERS)
  end
end

module IssueHarvester
  require "json"

  API_KEY = ENV.fetch("GITLAB_API_KEY")
  BASE_HEADERS = {
    "PRIVATE-TOKEN" => API_KEY,
    "Content-Type" => "application/json"
  }

  # GET /groups/:id/epics/:epic_iid/issues
  def harvest_close_issues_from_epic(group_id, epic_id)
    url = "https://gitlab.com/api/v4//groups/#{group_id}/epics/#{epic_id}/issues"

    Enumerator.new do |yielder|
      page = 1

      loop do
        results = HTTParty.get(url + "?page=#{page}",
          follow_redirects: true,
          maintain_method_across_redirects: true,
          headers: BASE_HEADERS)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          raise StopIteration
        end
      end
    end.lazy
  end
end

class MeanTimeToMerge
  require "fast_jsonparser"
  require "date"
  require "tty/table"
  require "csv"
  prepend MRHarvester
  prepend IssueHarvester

  MergeRequest = Struct.new(:iid, :created_at, :merged_at, :state, :days_to_merge, :url)
  Issue = Struct.new(:iid, :project_id, :title, :web_url)

  def run!
    CSV.open("merge_requests.csv", "w") do |csv|
      issues = harvest_close_issues_from_epic(9970, 4547).map { |response| Issue.new(response["iid"], response["project_id"], response["title"], response["web_url"]) }
      issues.each do |issue|
        puts "#{issue.iid}: #{issue.title} (#{issue.web_url})"

        mr_json = harvest_related_merged_requests(issue.project_id, issue.iid).response.body
        merge_requests = FastJsonparser.parse(mr_json)
        table = TTY::Table.new

        merge_requests.each do |merge_request|
          next if merge_request[:state] != "merged"
          # puts MergeRequest.new(merge_request[:iid], merge_request[:created_at], merge_request[:merged_at], merge_request[:state], days_between(merge_request[:created_at], merge_request[:merged_at], merge_request[:web_url])).inspect
          table << [merge_request[:iid], merge_request[:created_at], merge_request[:merged_at], merge_request[:state], days_between(merge_request[:created_at], merge_request[:merged_at]), merge_request[:web_url]]
          csv << [merge_request[:iid], merge_request[:web_url], days_between(merge_request[:created_at], merge_request[:merged_at])]
        end

        puts table.render(:ascii)
      end
    end
  end

  private

  def days_between(start, stop)
    (Date.parse(start)...Date.parse(stop)).count
  end
end

MeanTimeToMerge.new.run!
