require "perfect_toml"
require "httparty"
require "tty-spinner"
require "pastel"
require "date"
require "terminal-table"

class Report
  API_KEY = ENV.fetch("GITLAB_API_KEY")
  TEAM = PerfectTOML.load_file("../../team.toml", symbolize_names: true)

  Engineer = Struct.new(:name, :activity, :issues, :merge_requests) do
    def to_s
      name.to_s
    end

    def activity_by_month
      monthly_activity = {}
      activity.group_by { |d, n| Date.parse(d).strftime("%Y-%m") }.each do |(k, v)|
        monthly_activity.merge!(k => v.map { |month_activity| month_activity[1] }.reduce(:+))
      end
      monthly_activity
    end

    def merge_requests_by_month
      mr_calendar = {}
      merge_requests.group_by { |mr| Date.parse(mr.merged_at).strftime("%Y-%m") }.each do |(k, v)|
        mr_calendar.merge!(k => v.size)
      end
      mr_calendar
    end

    def issue_weight_by_month
      issue_calendar = {}
      issues.group_by { |i| Date.parse(i.created_at).strftime("%Y-%m") }.each do |(k, v)|
        issue_calendar.merge!(k => v.sum { |i| i.weight.to_i })
      end
      issue_calendar
    end
  end

  MergeRequest = Struct.new(:title, :url, :created_at, :merged_at, :state, :author)

  Issue = Struct.new(:title, :url, :created_at, :weight)

  attr_accessor :engineers
  def initialize
    @engineers = TEAM[:members].map { |e| Engineer.new(name: e) }
  end

  def setup
    pastel = Pastel.new
    spinners = TTY::Spinner::Multi.new("[:spinner] Gathering information...")
    @engineers.each do |engineer|
      spinners.register("[:spinner] #{engineer.name}") do |sp|
        calendar_activity(engineer)
        merge_request_activity(engineer)
        issue_activity(engineer)
        sp.success(pastel.green("success"))
      end
    end
    spinners.auto_spin
  end

  def run!
    setup
    last_six_months = (0..5).map { |i| (Date.today << i).strftime("%Y-%m") }.reverse

    puts
    puts "Team productivity report!"
    puts "========================"
    puts

    %i[activity_by_month merge_requests_by_month issue_weight_by_month].each do |method|
      title = case method
      when :activity_by_month
        "Activity by Month"
      when :merge_requests_by_month
        "Merge Requests by Month"
      when :issue_weight_by_month
        "Issue Weight by Month"
      end

      headings = ["Engineer Name"] + last_six_months + ["Total"]
      rows = @engineers.map do |engineer|
        row = [engineer.name]
        monthly_data = engineer.send(method)
        # puts "#{title}: EngineerName #{monthly_data}"
        sum = 0
        last_six_months.each do |month|
          row << (monthly_data[month] || 0)
          sum += monthly_data[month] || 0
          # puts "#{title}: EngineerName #{engineer.name}, trying for #{month}: Got #{row}"
        end
        row << sum

        row
      end

      table = Terminal::Table.new(title: title, headings: headings, rows: rows)
      puts table
      puts
    end
  end

  def self.run!
    new.run!
  end

  def calendar_activity(engineer)
    calendar_url = "https://gitlab.com/users/#{engineer}/calendar.json"
    calendar_response = HTTParty.get(calendar_url)
    engineer.activity = calendar_response.parsed_response
  end

  def merge_request_activity(engineer)
    mrs = []
    merge_request_url = "https://gitlab.com/api/v4/merge_requests"
    base_query = {
      "author_username" => "",
      "created_after" => DateTime.parse(engineer.activity.first.first).iso8601, # 2020-01-01T00:00:00Z
      "scope" => "all",
      "state" => "merged"
    }
    base_headers = {"PRIVATE-TOKEN" => API_KEY}

    request = Enumerator.new do |yielder|
      page = 1

      loop do
        results = HTTParty.get(merge_request_url,
          query: base_query.merge(author_username: engineer.name, page: page, per_page: 100),
          follow_redirects: true,
          maintain_method_across_redirects: true,
          headers: base_headers)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          # puts "Stopping at page #{page} for #{engineer.name}"
          # puts results.code
          raise StopIteration
        end
      end
    end.lazy

    request.to_a.each do |mr|
      mrs << MergeRequest.new(
        title: mr["title"],
        url: mr["web_url"],
        created_at: mr["created_at"],
        merged_at: mr["merged_at"],
        state: mr["state"],
        author: mr["author"]["username"]
      )
    end

    engineer.merge_requests = mrs
  end

  def issue_activity(engineer)
    closed_issues = []
    merge_request_url = "https://gitlab.com/api/v4/issues"
    base_query = {
      "scope" => "all",
      "state" => "closed"
    }
    base_headers = {"PRIVATE-TOKEN" => API_KEY}

    request = Enumerator.new do |yielder|
      page = 1

      loop do
        results = HTTParty.get(merge_request_url,
          query: base_query.merge(assignee_username: [engineer.name], page: page, per_page: 100),
          follow_redirects: true,
          maintain_method_across_redirects: true,
          headers: base_headers)

        if results.success? && !JSON.parse(results.body).empty?
          results.map { |item| yielder << item }
          page += 1
        else
          # puts "Stopping at page #{page} for #{engineer.name}"
          # puts results.code
          raise StopIteration
        end
      end
    end.lazy

    request.to_a.each do |issue|
      closed_issues << Issue.new(
        title: issue["title"],
        url: issue["web_url"],
        created_at: issue["closed_at"],
        weight: issue["weight"]
      )
    end
    # puts "#{closed_issues.inspect}"

    engineer.issues = closed_issues
  end
end

Report.run!
