require "perfect_toml"

class Engineer
  TEAM = PerfectTOML.load_file("team.toml", symbolize_names: true)

  attr_reader :engineers
  def initialize
    @engineers = TEAM[:memebers].freeze
  end
end
