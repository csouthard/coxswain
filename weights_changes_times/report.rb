require "csv"
require_relative "lib/gitlab"

class Report
  include Gitlab
  client = Gitlab::Client.new
  Stat = Struct.new(:iid, :project_id, :weight, :changes_count, :time_to_merge)
  stats = []
  weights = [1, 2, 3, 5, 8, 13]

  weights.each do |weight|
    issues = client.issues.list(params: {"scope" => "all", "state" => "closed", "weight" => weight})
    issues.each do |issue|
      client.issue_merge_requests.list(issue.project_id, issue.iid)&.each do |related_mr|
        mr = client.merge_requests.find(issue.project_id, related_mr["iid"])
        puts mr
        next if mr.merged_at.nil?
        stats << Stat.new(mr.iid, issue.project_id, weight, mr.changes_count, (Date.parse(mr.created_at)...Date.parse(mr.merged_at)).count)
      end
    end
  end

  puts stats

  csv_headers = ["iid", "project_id", "weight", "changed_count", "time_to_merge"]
  CSV.open("merge_request_stats.csv", "a+") do |csv|
    csv << csv_headers
    stats.each do |stat|
      csv << CSV::Row.new(csv_headers, [stat.iid, stat.project_id, stat.weight, stat.changes_count, stat.time_to_merge])
    end
  end
end
