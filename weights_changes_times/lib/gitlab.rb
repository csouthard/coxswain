require "httparty"

module Gitlab
  require_relative "gitlab/client"
  require_relative "gitlab/resources/issues"
  require_relative "gitlab/resources/issue_merge_requests"
  require_relative "gitlab/resources/merge_request"
  require_relative "gitlab/resources/epic_issues"
  require_relative "gitlab/resources/epic_linked_epics"
end
