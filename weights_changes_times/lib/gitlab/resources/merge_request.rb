require_relative "../../../lib/resource"

module Gitlab
  class MergeRequestResource < Resource
    MergeRequest = Struct.new(:id, :iid, :project_id, :title, :web_url, :state, :created_at, :merged_at, :changes_count)

    # GET /projects/:id/merge_requests/:merge_request_iid
    def find(project_id, merge_request_iid)
      path = "/projects/#{project_id}/merge_requests/#{merge_request_iid}"
      puts path
      response = get_without_params_request(path)
      MergeRequest.new(response["id"], response["iid"], response["project_id"], response["title"], response["web_url"], response["state"], response["created_at"], response["merged_at"], response["changes_count"])
    end
  end
end
