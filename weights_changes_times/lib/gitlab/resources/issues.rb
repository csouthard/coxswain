require_relative "../../../lib/resource"

module Gitlab
  class IssuesResource < Resource
    Issue = Struct.new(:iid, :project_id, :title, :web_url, :labels, :assignees, :weight)

    def list(params: {}, headers: {})
      get_request("/groups/9970/issues?", params: params, headers: headers).map { |response|
        Issue.new(response["iid"], response["project_id"], response["title"], response["web_url"], response["labels"], response["assignees"], response["weight"])
      }.to_a
    end
  end
end
