require_relative "../../../lib/resource"

module Gitlab
  class EpicLinkedEpicsResource < Resource
    Epic = Struct.new(:id, :iid, :title, :labels, :group_id, :parent_id, keyword_init: true)

    # GET /groups/:id/epics/:epic_iid/epics
    def list(group_id, epic_id)
      path = "/groups/#{group_id}/epics/#{epic_id}/epics"
      puts path
      get_without_params_request(path).map { |response|
        Epic.new(id: response["id"], iid: response["iid"], title: response["title"], labels: response["labels"], group_id: group_id, parent_id: response["parent_id"])
      }.to_a
    end
  end
end
