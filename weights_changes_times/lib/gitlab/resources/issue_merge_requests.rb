require_relative "../../../lib/resource"

module Gitlab
  class IssueMergeRequestsResource < Resource
    MergeRequest = Struct.new(:id, :iid, :project_id, :title, :web_url, :state, :created_at, :merged_at, :changes_count)

    # GET /projects/:id/issues/:issue_iid/related_merge_requests
    def list(project_id, issue_iid)
      path = "/projects/#{project_id}/issues/#{issue_iid}/related_merge_requests"
      puts path
      get_without_params_request(path).map { |response|
        MergeRequest.new(response["id"], response["iid"], response["project_id"], response["title"], response["web_url"], response["state"], response["created_at"], response["merged_at"], response["changes_count"])
      }.to_a
    end
  end
end
