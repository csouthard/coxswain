require_relative "../../../lib/resource"

module Gitlab
  class EpicIssuesResource < Resource
    Issue = Struct.new(:id, :iid, :title, :web_url, :labels, :assignees, :weight, :milestone, :state, :group_id, :epic_id, :project_id)

    # GET /groups/:id/epics/:epic_iid/issues
    def list(group_id, epic_id)
      path = "/groups/#{group_id}/epics/#{epic_id}/issues"
      puts path
      get_without_params_request(path).map { |response|
        Issue.new(response["id"], response["iid"], response["title"], response["web_url"], response["labels"], response["assignees"], response["weight"], response["milestone"], response["state"], group_id, epic_id, response["project_id"])
      }.to_a
    end
  end
end
