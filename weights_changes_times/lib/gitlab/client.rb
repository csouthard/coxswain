require "bundler/setup"
require "dotenv"
require "httparty"
require "erb"
require_relative "../utils"

module Gitlab
  class Client
    Dotenv.load(File.expand_path("../../.env", __dir__))

    Dotenv.require_keys("PRIVATE_TOKEN")

    API_KEY = ENV.fetch("PRIVATE_TOKEN")
    BASE_URL = "https://gitlab.com/api/v4"
    BASE_QUERY = {
      # "author_username" => "",
      # "updated_after" => "", # 2020-01-01T00:00:00Z
      # "scope" => "all"
    }
    BASE_HEADERS = {"PRIVATE-TOKEN" => API_KEY}
    VERBOSE_MODE = ENV["VERBOSE"] || "false"
    DRY_RUN = ENV["DRY_RUN"] || "false"

    def initialize(stage = "section::fulfillment", team = "group::utilization", other = "", options = {})
      @stage = stage
      @team = team
      @other = other
      @options = options
      @data = []
    end

    def issues
      IssuesResource.new(self)
    end

    def issue_merge_requests
      IssueMergeRequestsResource.new(self)
    end

    def merge_requests
      MergeRequestResource.new(self)
    end

    def epic_issues
      EpicIssuesResource.new(self)
    end

    def epic_epics
      EpicLinkedEpicsResource.new(self)
    end

    def get_without_params(path, headers: {})
      response = HTTParty.get(full_path(path, {}),
        follow_redirects: true,
        maintain_method_across_redirects: true,
        headers: headers.nil? ? BASE_HEADERS : BASE_HEADERS.merge(headers))
      puts response.body if VERBOSE_MODE == "true"
      response
    end

    def get(path, page = 1, query_params: {}, headers: {})
      query_options = base_options.merge(page: page).merge(compute_query_params).merge(query_params)

      # if DRY_RUN
      #   puts full_path(path, query_options)
      #   abort
      # end

      response = HTTParty.get(full_path(path, query_options),
        follow_redirects: true,
        maintain_method_across_redirects: true,
        headers: headers.nil? ? BASE_HEADERS : BASE_HEADERS.merge(headers))
      puts response.body if VERBOSE_MODE == "true"
      response
    end

    def get_paginated_results(path, query_params, headers)
      Enumerator.new do |yielder|
        page = 1

        loop do
          results = get(path, page, query_params, headers)

          if results.success? && !JSON.parse(results.body).empty?
            results.map { |item| yielder << item }
            page += 1
          else
            raise StopIteration
          end
        end
      end.lazy
    end

    def base_options
      {per_page: 100, page: 1}
    end

    def full_path(path, query_options)
      if query_options.empty?
        "#{BASE_URL}#{path}"
      else
        "#{BASE_URL}#{path}&#{Utils.format_http_params(query_options)}"
      end
      # if VERBOSE_MODE == "true"
      #   puts path
      # end
    end

    def compute_query_params
      BASE_QUERY.merge({"labels" => [@stage, @team, @other].compact.join(",")}, @options)
    end
  end
end
