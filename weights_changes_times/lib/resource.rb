module Gitlab
  class Resource
    attr_reader :client

    def initialize(client)
      @client = client
    end

    private

    def get_request(url, params: {}, headers: {})
      client.get(url, query_params: params, headers: headers)
    end

    def get_without_params_request(url, headers: {})
      client.get_without_params(url, headers: headers)
    end
  end
end
