require "csv"
require_relative "lib/gitlab"

class EpicsAndIssues
  include Gitlab

  attr_reader :epics
  attr_accessor :client

  def initialize
    @client = Gitlab::Client.new
  end

  def epic_issues(epics)
    epics.map { |epic| @client.epic_issues.list(epic.group_id, epic.iid) }
  end

  def epic_epics(epics)
    epics.map { |epic| @client.epic_epics.list(epic.group_id, epic.iid) }
  end
end

gitlab_org = 9970
SimpleEpic = Struct.new(:group_id, :iid)
epic_list = [10105, 10101, 10100, 10104, 10103, 9173, 10095, 10096, 10097, 10098, 10099, 7840, 10106, 7446, 9912, 8542]
epics = epic_list.map { |iid| SimpleEpic.new(gitlab_org, iid) }

eni = EpicsAndIssues.new

child_epics = eni.epic_epics(epics).compact.flatten
all_epics = epics.push(*child_epics).uniq{ |epic| epic.iid }

all_issues = eni.epic_issues(all_epics).compact.flatten

csv_headers = ["id", "iid", "title", "web_url", "labels", "assignees", "weight", "milestone", "state", "epic_id", "group_id", "project_id"]
CSV.open("epic_issues.csv", "a+") do |csv|
  csv << csv_headers
  all_issues.each do |issue|
    csv << CSV::Row.new(csv_headers, [issue.id, issue.iid, issue.title, issue.web_url, issue.labels, issue.assignees&.map { |assignee| assignee["username"] }&.join(", "), issue.weight, issue.milestone&.fetch("title"), issue.state, issue.epic_id, issue.group_id, issue.project_id])
  end
end
